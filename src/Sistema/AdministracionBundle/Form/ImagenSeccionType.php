<?php

namespace Sistema\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ImagenSeccionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', \A2lix\TranslationFormBundle\Form\Type\TranslationsType::class, array(
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => TextType::class,
                            'label' => false,
                        )
                    )
                ))
                ->add('imagenFile', VichImageType::class, array(
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdministracionBundle\Entity\ImagenSeccion',
        ));
    }

}
