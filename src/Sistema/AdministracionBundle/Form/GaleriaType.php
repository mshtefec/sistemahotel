<?php

namespace Sistema\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sistema\AdministracionBundle\Form\ImagenGaleriaType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GaleriaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('translations', \A2lix\TranslationFormBundle\Form\Type\TranslationsType::class, array(
                'fields' => array(
                    'titulo' => array(
                        'field_type' => TextType::class, 
                        'label' => 'Titulo', 
                    ),
                    'descripcion' => array(
                        'field_type' => CKEditorType::class, 
                        'label' => 'descripcion',
                    )
                )
            )) 
            ->add('imagenes', CollectionType::class, array(
                'label' => false,
                'entry_type' => ImagenGaleriaType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,    
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdministracionBundle\Entity\Galeria',
            'cascade_validation' => true
        ));
    }
}
