<?php

namespace Sistema\AdministracionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * SeccionFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class SeccionFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', Filters\TextFilterType::class, array(
                'condition_pattern' => FilterOperands::OPERAND_SELECTOR,
            ))
            ->add('descripcion', Filters\TextFilterType::class, array(
                'condition_pattern' => FilterOperands::OPERAND_SELECTOR,
            ))
            ->add('ruta', Filters\TextFilterType::class, array(
                'condition_pattern' => FilterOperands::OPERAND_SELECTOR,
            ))
            ->add('tituloMenu', Filters\TextFilterType::class, array(
                'condition_pattern' => FilterOperands::OPERAND_SELECTOR,
            ))
            ->add('activo', Filters\BooleanFilterType::class)
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdministracionBundle\Entity\Seccion'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'sistema_administracionbundle_seccionfiltertype';
    }
}
