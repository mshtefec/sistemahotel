<?php

namespace Sistema\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sistema\AdministracionBundle\Entity\Seccion;
use Sistema\AdministracionBundle\Entity\ImagenSeccion;
use Sistema\AdministracionBundle\Form\SeccionType;
use Sistema\AdministracionBundle\Form\SeccionFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * Seccion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/seccion")
 */
class SeccionController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdministracionBundle/Resources/config/Seccion.yml',
    );

    /**
     * Lists all Seccion entities.
     *
     * @Route("/", name="admin_seccion")
     * @Method("GET")
     */
    public function indexAction(Request $request) {
        $this->config['filterType'] = SeccionFilterType::class;
        $response = parent::indexAction($request);

        return $response;
    }

    /**
     * Creates a new Seccion entity.
     *
     * @Route("/", name="admin_seccion_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $this->config['newType'] = SeccionType::class;
        $response = parent::createAction($request);

        return $response;
    }

    /**
     * Displays a form to create a new Seccion entity.
     *
     * @Route("/new", name="admin_seccion_new")
     * @Method("GET")
     */
    public function newAction() {
        $this->config['newType'] = SeccionType::class;

        $config = $this->getConfig();
        $entity = new $config['entity']();
        $imagen = new ImagenSeccion();
        $entity->addImagen($imagen);
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return $this->render($config['view_new'], array(
                    'config' => $config,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_show")
     * @Method("GET")
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Seccion entity.
     *
     * @Route("/{id}/edit", name="admin_seccion_edit")
     * @Method("GET")
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = SeccionType::class;
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_update")
     * @Method("PUT")
     * @I18nDoctrine
     */
    public function updateAction(Request $request, $id) {
        $this->config['editType'] = SeccionType::class;
        $response = parent::updateAction($request, $id);

        return $response;
    }

    /**
     * Deletes a Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $response = parent::deleteAction($request, $id);

        return $response;
    }

    public function menuAction() {

        $config = $this->getConfig();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->findAll();

        return $this->render('SistemaAdministracionBundle:Form:macro-menu.html.twig', array(
                    'secciones' => $entity
                        )
        );
    }

    /**
     * Exporter Seccion.
     *
     * @Route("/exporter/{format}", name="admin_seccion_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
