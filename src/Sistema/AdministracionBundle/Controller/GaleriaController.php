<?php

namespace Sistema\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sistema\AdministracionBundle\Entity\Galeria;
use Sistema\AdministracionBundle\Entity\ImagenGaleria;
use Sistema\AdministracionBundle\Form\GaleriaType;
use Sistema\AdministracionBundle\Form\GaleriaFilterType;

/**
 * Galeria controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/galeria")
 */
class GaleriaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdministracionBundle/Resources/config/Galeria.yml',
    );

    /**
     * Lists all Galeria entities.
     *
     * @Route("/", name="admin_galeria")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $this->config['filterType'] = GaleriaFilterType::class;
        $response = parent::indexAction($request);

        return $response;
    }

    /**
     * Creates a new Galeria entity.
     *
     * @Route("/", name="admin_galeria_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $this->config['newType'] = GaleriaType::class;
        $response = parent::createAction($request);

        return $response;
    }

    /**
     * Displays a form to create a new Galeria entity.
     *
     * @Route("/new", name="admin_galeria_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $this->config['newType'] = GaleriaType::class;
        
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $imagen = new ImagenGaleria();
        $entity->addImagen($imagen);
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return $this->render($config['view_new'], array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Galeria entity.
     *
     * @Route("/{id}", name="admin_galeria_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Galeria entity.
     *
     * @Route("/{id}/edit", name="admin_galeria_edit")
     * @Method("GET")
     */
    public function editAction($id)
    {
        $this->config['editType'] = GaleriaType::class;
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Galeria entity.
     *
     * @Route("/{id}", name="admin_galeria_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
        $this->config['editType'] = GaleriaType::class;
        $response = parent::updateAction($request, $id);

        return $response;
    }

    /**
     * Deletes a Galeria entity.
     *
     * @Route("/{id}", name="admin_galeria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $response = parent::deleteAction($request, $id);

        return $response;
    }

    /**
     * Exporter Galeria.
     *
     * @Route("/exporter/{format}", name="admin_galeria_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }
}
