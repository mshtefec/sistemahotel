<?php

namespace Sistema\AdministracionBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Seccion
 *
 * @ORM\Table(name="seccion")
 * @ORM\Entity(repositoryClass="Sistema\AdministracionBundle\Repository\SeccionRepository")
 */
class Seccion {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^[-._=A-z0-9]*(([-._=])*[-._=A-z0-9])*$/",
     *     match=true,
     *     message="La ruta solo puede contener letras, numeros, y puntos."
     * )
     */
    private $ruta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\AdministracionBundle\Entity\ImagenSeccion", mappedBy="seccion", cascade={"persist","remove"}, orphanRemoval=true)
     * @Assert\Valid
     */
    private $imagenes;


    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->imagenes     = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     *
     * @return Seccion
     */
    public function setRuta($ruta) {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string
     */
    public function getRuta() {
        return $this->ruta;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Seccion
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Get imagenes
     *
     * @return ImagenSeccion[]|ArrayCollection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add imagen
     *
     * @param \Sistema\AdministracionBundle\Entity\ImagenSeccion $imagen
     *
     * @return Seccion
     */
    public function addImagen(ImagenSeccion $imagen) {
        $this->imagenes[] = $imagen;
        $imagen->setSeccion($this);

        return $this;
    }

    /**
     * Remove imagen
     *
     * @param \Sistema\AdministracionBundle\Entity\ImagenSeccion $imagen
     */
    public function removeImagen(\Sistema\HotelBundle\Entity\ImagenSeccion $imagen) {
        $this->imagenes->removeElement($imagen);
    }

}
