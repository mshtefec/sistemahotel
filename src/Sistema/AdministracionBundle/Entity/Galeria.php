<?php

namespace Sistema\AdministracionBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Galeria
 *
 * @ORM\Table(name="galeria")
 * @ORM\Entity(repositoryClass="Sistema\AdministracionBundle\Repository\GaleriaRepository")
 */
class Galeria
{
    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\AdministracionBundle\Entity\ImagenGaleria", mappedBy="galeria", cascade={"persist","remove"}, orphanRemoval=true)
     * @Assert\Valid
     */
    private $imagenes;

    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get imagenes
     *
     * @return ImagenGaleria[]|ArrayCollection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add imagen
     *
     * @param \Sistema\AdministracionBundle\Entity\ImagenGaleria $imagen
     *
     * @return Seccion
     */
    public function addImagen(ImagenGaleria $imagen) {
        $this->imagenes[] = $imagen;
        $imagen->setGaleria($this);

        return $this;
    }

    /**
     * Remove imagen
     *
     * @param \Sistema\AdministracionBundle\Entity\ImagenGaleria $imagen
     */
    public function removeImagen(\Sistema\HotelBundle\Entity\ImagenGaleria $imagen) {
        $this->imagenes->removeElement($imagen);
    }
}

