<?php

namespace Sistema\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ImagenGaleria
 *
 * @ORM\Table(name="imagen_galeria")
 * @ORM\Entity(repositoryClass="Sistema\AdministracionBundle\Repository\ImagenGaleriaRepository")
 * @Vich\Uploadable
 */
class ImagenGaleria {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotNull()
     */
    private $imagen;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="imagen_galeria", fileNameProperty="imagen")
     */
    private $imagenFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255)
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\AdministracionBundle\Entity\Galeria", inversedBy="imagenes")
     * @ORM\JoinColumn(name="galeria_id", referencedColumnName="id", nullable=false)
     */
    private $galeria;

    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function setImagenFile(File $imagen = null) {
        $this->imagenFile = $imagen;

        if ($imagen) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImagenFile() {
        return $this->imagenFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ImagenGaleria
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set galeria
     *
     * @param \AppBundle\Entity\Galeria $galeria
     *
     * @return ImagenGaleria
     */
    public function setgaleria(\Sistema\AdministracionBundle\Entity\Galeria $galeria = null) {
        $this->galeria = $galeria;

        return $this;
    }

    /**
     * Get galeria
     *
     * @return \AppBundle\Entity\Galeria
     */
    public function getgaleria() {
        return $this->galeria;
    }

}
