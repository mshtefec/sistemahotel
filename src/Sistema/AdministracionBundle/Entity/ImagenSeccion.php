<?php

namespace Sistema\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ImagenSeccion
 *
 * @ORM\Table(name="imagen_seccion")
 * @ORM\Entity(repositoryClass="Sistema\AdministracionBundle\Repository\ImagenSeccionRepository")
 * @Vich\Uploadable
 */
class ImagenSeccion {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="imagen_seccion", fileNameProperty="imagen")
     */
    private $imagenFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255)
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\AdministracionBundle\Entity\Seccion", inversedBy="imagenes")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id")
     */
    private $seccion;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function setImagenFile(File $imagen = null) {
        $this->imagenFile = $imagen;

        if ($imagen) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImagenFile() {
        return $this->imagenFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ImagenHabitacion
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set seccion
     *
     * @param \AppBundle\Entity\Seccion $seccion
     *
     * @return ImagenSeccion
     */
    public function setSeccion(\Sistema\AdministracionBundle\Entity\Seccion $seccion = null) {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion
     *
     * @return \AppBundle\Entity\Seccion
     */
    public function getSeccion() {
        return $this->seccion;
    }

}
