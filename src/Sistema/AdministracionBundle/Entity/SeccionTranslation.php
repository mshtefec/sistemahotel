<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sistema\AdministracionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of SeccionTranslation
 *
 * @author rodrigo
 */

/**
 * @ORM\Entity
 */
class SeccionTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_menu", type="string", length=255, nullable=true)
     */
    private $tituloMenu;

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return SeccionTranslation
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return SeccionTranslation
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }


    /**
     * Set tituloMenu
     *
     * @param string $tituloMenu
     *
     * @return SeccionTranslation
     */
    public function setTituloMenu($tituloMenu)
    {
        $this->tituloMenu = $tituloMenu;

        return $this;
    }

    /**
     * Get tituloMenu
     *
     * @return string
     */
    public function getTituloMenu()
    {
        return $this->tituloMenu;
    }
}
