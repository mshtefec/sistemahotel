<?php

namespace Sistema\MotorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reserva
 *
 * @ORM\Table(name="reserva")
 * @ORM\Entity(repositoryClass="Sistema\MotorBundle\Repository\ReservaRepository")
 */
class Reserva {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_desde", type="date")
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_hasta", type="date")
     */
    private $fechaHasta;

    /**
     * @var string
     *
     * @ORM\Column(name="forma_pago", type="string", length=255)
     */
    private $formaPago;

    /**
     * @var float
     *
     * @ORM\Column(name="monto_total", type="float")
     */
    private $montoTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255, unique=true)
     */
    private $codigo;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\MotorBundle\Entity\DatosContacto", inversedBy="reservas")
     * @ORM\JoinColumn(name="contacto_id", referencedColumnName="id")
     */
    private $contacto;

    /**
     * @var integer
     *
     * @ORM\Column(name="adultos", type="integer")
     */
    private $adultos;

    /**
     * @var integer
     *
     * @ORM\Column(name="menores", type="integer")
     */
    private $menores;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\HotelBundle\Entity\TipoHabitacion", inversedBy="reservas")
     * @ORM\JoinColumn(name="tipoHabitacion_id", referencedColumnName="id")
     */
    protected $tipoHabitacion;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return Reserva
     */
    public function setFechaDesde($fechaDesde) {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde() {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return Reserva
     */
    public function setFechaHasta($fechaHasta) {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta() {
        return $this->fechaHasta;
    }

    /**
     * Set formaPago
     *
     * @param string $formaPago
     *
     * @return Reserva
     */
    public function setFormaPago($formaPago) {
        $this->formaPago = $formaPago;

        return $this;
    }

    /**
     * Get formaPago
     *
     * @return string
     */
    public function getFormaPago() {
        return $this->formaPago;
    }

    /**
     * Set montoTotal
     *
     * @param float $montoTotal
     *
     * @return Reserva
     */
    public function setMontoTotal($montoTotal) {
        $this->montoTotal = $montoTotal;

        return $this;
    }

    /**
     * Get montoTotal
     *
     * @return float
     */
    public function getMontoTotal() {
        return $this->montoTotal;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Reserva
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set contacto
     *
     * @param \Sistema\MotorBundle\Entity\DatosContacto $contacto
     *
     * @return Reserva
     */
    public function setContacto(\Sistema\MotorBundle\Entity\DatosContacto $contacto = null) {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return \Sistema\MotorBundle\Entity\DatosContacto
     */
    public function getContacto() {
        return $this->contacto;
    }

    /**
     * Set adultos
     *
     * @param integer $adultos
     *
     * @return Reserva
     */
    public function setAdultos($adultos) {
        $this->adultos = $adultos;

        return $this;
    }

    /**
     * Get adultos
     *
     * @return integer
     */
    public function getAdultos() {
        return $this->adultos;
    }

    /**
     * Set menores
     *
     * @param integer $menores
     *
     * @return Reserva
     */
    public function setMenores($menores) {
        $this->menores = $menores;

        return $this;
    }

    /**
     * Get menores
     *
     * @return integer
     */
    public function getMenores() {
        return $this->menores;
    }

    /**
     * Set tipoHabitacion
     *
     * @param \Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion
     *
     * @return Reserva
     */
    public function setTipoHabitacion(\Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion = null) {
        $this->tipoHabitacion = $tipoHabitacion;

        return $this;
    }

    /**
     * Get tipoHabitacion
     *
     * @return \Sistema\HotelBundle\Entity\TipoHabitacion
     */
    public function getTipoHabitacion() {
        return $this->tipoHabitacion;
    }

}
