<?php

namespace Sistema\HotelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sistema\HotelBundle\Entity\TipoHabitacion;
use Sistema\HotelBundle\Form\TipoHabitacionType;
use Sistema\HotelBundle\Form\TipoHabitacionFilterType;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

/**
 * TipoHabitacion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/tipoHabitacion")
 */
class TipoHabitacionController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/HotelBundle/Resources/config/TipoHabitacion.yml',
    );

    /**
     * Lists all TipoHabitacion entities.
     *
     * @Route("/", name="admin_tipoHabitacion")
     * @Method("GET")
     */
    public function indexAction(Request $request) {
        $this->config['filterType'] = TipoHabitacionFilterType::class;
        $response = parent::indexAction($request);

        return $response;
    }

    /**
     * Creates a new TipoHabitacion entity.
     *
     * @Route("/", name="admin_tipoHabitacion_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $this->config['newType'] = TipoHabitacionType::class;
        $response = parent::createAction($request);

        return $response;
    }

    /**
     * Displays a form to create a new TipoHabitacion entity.
     *
     * @Route("/new", name="admin_tipoHabitacion_new")
     * @Method("GET")
     */
    public function newAction() {
        $this->config['newType'] = TipoHabitacionType::class;
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a TipoHabitacion entity.
     *
     * @Route("/{id}", name="admin_tipoHabitacion_show")
     * @Method("GET")
     * @I18nDoctrine
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing TipoHabitacion entity.
     *
     * @Route("/{id}/edit", name="admin_tipoHabitacion_edit")
     * @Method("GET")
     * @I18nDoctrine
     */
    public function editAction($id) {
        $this->config['editType'] = TipoHabitacionType::class;
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing TipoHabitacion entity.
     *
     * @Route("/{id}", name="admin_tipoHabitacion_update")
     * @Method("PUT")
     * @I18nDoctrine
     */
    public function updateAction(Request $request, $id) {
        $this->config['editType'] = TipoHabitacionType::class;
        $response = parent::updateAction($request, $id);

        return $response;
    }

    /**
     * Deletes a TipoHabitacion entity.
     *
     * @Route("/{id}", name="admin_tipoHabitacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $response = parent::deleteAction($request, $id);

        return $response;
    }

    /**
     * Exporter TipoHabitacion.
     *
     * @Route("/exporter/{format}", name="admin_tipoHabitacion_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a TipoHabitacion entity.
     *
     * @Route("/autocomplete-forms/get-imagenes", name="TipoHabitacion_autocomplete_imagenes")
     */
    public function getAutocompleteImagenHabitacion(Request $request) {
        $options = array(
            'repository' => "SistemaHotelBundle:ImagenHabitacion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($request, $options);

        return $response;
    }

    /**
     * Autocomplete a TipoHabitacion entity.
     *
     * @Route("/autocomplete-forms/get-reservas", name="TipoHabitacion_autocomplete_reservas")
     */
    public function getAutocompleteReserva(Request $request) {
        $options = array(
            'repository' => "SistemaMotorBundle:Reserva",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($request, $options);

        return $response;
    }

    /**
     * Autocomplete a TipoHabitacion entity.
     *
     * @Route("/autocomplete-forms/get-habitacions", name="TipoHabitacion_autocomplete_habitacions")
     */
    public function getAutocompleteHabitacion(Request $request) {
        $options = array(
            'repository' => "SistemaHotelBundle:Habitacion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($request, $options);

        return $response;
    }

}
