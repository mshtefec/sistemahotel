<?php

namespace Sistema\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Habitacion
 *
 * @ORM\Table(name="habitacion")
 * @ORM\Entity(repositoryClass="Sistema\HotelBundle\Repository\HabitacionRepository")
 */
class Habitacion {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TipoHabitacion", inversedBy="habitacions")
     * @ORM\JoinColumn(name="tipoHabitacion_id", referencedColumnName="id")
     */
    private $tipoHabitacion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoHabitacion
     *
     * @param \Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion
     *
     * @return Habitacion
     */
    public function setTipoHabitacion(\Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion = null)
    {
        $this->tipoHabitacion = $tipoHabitacion;

        return $this;
    }

    /**
     * Get tipoHabitacion
     *
     * @return \Sistema\HotelBundle\Entity\TipoHabitacion
     */
    public function getTipoHabitacion()
    {
        return $this->tipoHabitacion;
    }
}
