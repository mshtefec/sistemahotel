<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sistema\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ImagenHabitacionTranslation
 *
 * @author rodrigo
 */

/**
 * @ORM\Entity
 */
class ImagenHabitacionTranslation implements \A2lix\I18nDoctrineBundle\Doctrine\Interfaces\OneLocaleInterface {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return ImagenHabitacionTranslation
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

}
