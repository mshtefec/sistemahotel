<?php

namespace Sistema\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TipoHabitacion
 *
 * @ORM\Table(name="tipo_habitacion")
 * @ORM\Entity(repositoryClass="Sistema\HotelBundle\Repository\TipoHabitacionRepository")
 */
class TipoHabitacion {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="tarifa", type="float")
     */
    private $tarifa;

    /**
     * @var float
     *
     * @ORM\Column(name="porcentajeDescuento", type="integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 99,
     *      minMessage = "Debe ingresar un numero mayor que {{ limit }}",
     *      maxMessage = "Debe ingresar un numero menor que {{ limit }}"
     * )
     */
    private $porcentajeDescuento;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="adultos", type="integer")
     */
    private $adultos;

    /**
     * @var integer
     *
     * @ORM\Column(name="menores", type="integer")
     */
    private $menores;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\HotelBundle\Entity\ImagenHabitacion", mappedBy="tipoHabitacion", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $imagenes;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\MotorBundle\Entity\Reserva", mappedBy="tipoHabitacion", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $reservas;

    /**
     * @ORM\OneToMany(targetEntity="Habitacion", mappedBy="tipoHabitacion", cascade={"persist","remove"})
     */
    private $habitacions;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reservas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->habitacions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tarifa
     *
     * @param float $tarifa
     *
     * @return TipoHabitacion
     */
    public function setTarifa($tarifa) {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * Get tarifa
     *
     * @return float
     */
    public function getTarifa() {
        return $this->tarifa;
    }

    /**
     * Set porcentajeDescuento
     *
     * @param integer $porcentajeDescuento
     *
     * @return TipoHabitacion
     */
    public function setPorcentajeDescuento($porcentajeDescuento) {
        $this->porcentajeDescuento = $porcentajeDescuento;

        return $this;
    }

    /**
     * Get porcentajeDescuento
     *
     * @return integer
     */
    public function getPorcentajeDescuento() {
        return $this->porcentajeDescuento;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return TipoHabitacion
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set adultos
     *
     * @param integer $adultos
     *
     * @return TipoHabitacion
     */
    public function setAdultos($adultos) {
        $this->adultos = $adultos;

        return $this;
    }

    /**
     * Get adultos
     *
     * @return integer
     */
    public function getAdultos() {
        return $this->adultos;
    }

    /**
     * Set menores
     *
     * @param integer $menores
     *
     * @return TipoHabitacion
     */
    public function setMenores($menores) {
        $this->menores = $menores;

        return $this;
    }

    /**
     * Get menores
     *
     * @return integer
     */
    public function getMenores() {
        return $this->menores;
    }

    /**
     * Add imagene
     *
     * @param \Sistema\HotelBundle\Entity\ImagenHabitacion $imagene
     *
     * @return TipoHabitacion
     */
    public function addImagene(\Sistema\HotelBundle\Entity\ImagenHabitacion $imagene) {
        $imagene->setTipoHabitacion($this);
        $this->imagenes[] = $imagene;

        return $this;
    }

    /**
     * Remove imagene
     *
     * @param \Sistema\HotelBundle\Entity\ImagenHabitacion $imagene
     */
    public function removeImagene(\Sistema\HotelBundle\Entity\ImagenHabitacion $imagene) {
        $this->imagenes->removeElement($imagene);
    }

    /**
     * Get imagenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add reserva
     *
     * @param \Sistema\MotorBundle\Entity\Reserva $reserva
     *
     * @return TipoHabitacion
     */
    public function addReserva(\Sistema\MotorBundle\Entity\Reserva $reserva) {
        $this->reservas[] = $reserva;

        return $this;
    }

    /**
     * Remove reserva
     *
     * @param \Sistema\MotorBundle\Entity\Reserva $reserva
     */
    public function removeReserva(\Sistema\MotorBundle\Entity\Reserva $reserva) {
        $reserva->setTipoHabitacion($this);
        $this->reservas->removeElement($reserva);
    }

    /**
     * Get reservas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservas() {
        return $this->reservas;
    }

    /**
     * Add habitacion
     *
     * @param \Sistema\HotelBundle\Entity\Habitacion $habitacion
     *
     * @return TipoHabitacion
     */
    public function addHabitacion(\Sistema\HotelBundle\Entity\Habitacion $habitacion) {
        $habitacion->setTipoHabitacion($this);
        $this->habitacions[] = $habitacion;

        return $this;
    }

    /**
     * Remove habitacion
     *
     * @param \Sistema\HotelBundle\Entity\Habitacion $habitacion
     */
    public function removeHabitacion(\Sistema\HotelBundle\Entity\Habitacion $habitacion) {
        $this->habitacions->removeElement($habitacion);
    }

    /**
     * Get habitacions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHabitacions() {
        return $this->habitacions;
    }

}
