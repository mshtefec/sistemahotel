<?php

namespace Sistema\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ImagenHabitacion
 *
 * @ORM\Table(name="imagen_habitacion")
 * @ORM\Entity(repositoryClass="Sistema\HotelBundle\Repository\ImagenHabitacionRepository")
 * @Vich\Uploadable
 */
class ImagenHabitacion {

    use \A2lix\I18nDoctrineBundle\Doctrine\ORM\Util\Translatable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $imagen;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="imagen_habitacion", fileNameProperty="imagen")
     */
    private $imagenFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255)
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\HotelBundle\Entity\TipoHabitacion", inversedBy="imagenes")
     * @ORM\JoinColumn(name="tipoHabitacion_id", referencedColumnName="id")
     */
    private $tipoHabitacion;
    protected $translations;

    /**
     * Constructor
     */
    public function __construct() {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function setImagenFile(File $imagen = null) {
        $this->imagenFile = $imagen;

        if ($imagen) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImagenFile() {
        return $this->imagenFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ImagenHabitacion
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set tipoHabitacion
     *
     * @param \Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion
     *
     * @return ImagenHabitacion
     */
    public function setTipoHabitacion(\Sistema\HotelBundle\Entity\TipoHabitacion $tipoHabitacion = null) {
        $this->tipoHabitacion = $tipoHabitacion;

        return $this;
    }

    /**
     * Get tipoHabitacion
     *
     * @return \Sistema\HotelBundle\Entity\TipoHabitacion
     */
    public function getTipoHabitacion() {
        return $this->tipoHabitacion;
    }

}
