<?php

namespace Sistema\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sistema\HotelBundle\Form\ImagenHabitacionType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TipoHabitacionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', \A2lix\TranslationFormBundle\Form\Type\TranslationsType::class, array(
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => TextType::class,
                            'label' => 'Nombre',
                        ),
                        'descripcion' => array(
                            'field_type' => CKEditorType::class,
                            'label' => 'Descripcion',
                        )
                    )
                ))
                ->add('tarifa')
                ->add('porcentajeDescuento')
                ->add('cantidad')
                ->add('adultos')
                ->add('menores')
                ->add('imagenes', CollectionType::class, array(
                    'label' => false,
                    'entry_type' => ImagenHabitacionType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\HotelBundle\Entity\TipoHabitacion'
        ));
    }

}
