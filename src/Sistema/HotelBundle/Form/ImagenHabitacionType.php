<?php

namespace Sistema\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ImagenHabitacionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('translations', \A2lix\TranslationFormBundle\Form\Type\TranslationsType::class, array(
                    'fields' => array(
                        'nombre' => array(
                            'field_type' => TextType::class,
                            'label' => false,
                        )
                    )
                ))
                /*
                  ->add('updatedAt', \SC\DatetimepickerBundle\Form\Type\DatetimeType::class, array('pickerOptions' =>
                  array(
                  'format'    => 'mm/dd/yyyy hh:ii',
                  'startView' => 'month',
                  'minView'   => 'hour',
                  'maxView'   => 'decade',
                  'todayBtn'  => true,
                  )))
                  //->add('habitacion')
                  ->add('habitacion', \MWSimple\Bundle\AdminCrudBundle\Form\Type\Select2entityType::class, array(
                  'class' => 'Sistema\HotelBundle\Entity\Habitacion',
                  'url'   => 'ImagenHabitacion_autocomplete_habitacion',
                  'configs' => array(
                  'multiple' => false,//required true or false
                  'width'    => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                ->add('imagenFile', VichImageType::class, array(
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\HotelBundle\Entity\ImagenHabitacion'
        ));
    }

}
