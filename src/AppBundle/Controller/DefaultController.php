<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller {

    /**
     * @Route("/homepage", name="homepage")
     */
    public function indexAction() {

        return $this->render('AppBundle:Default:index.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction() {

        return $this->render('AppBundle:Default:contact.html.twig');
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutAction() {

        return $this->render('AppBundle:Default:about.html.twig');
    }

    /**
     * @Route("/reservation", name="reservation")
     */
    public function reservationAction() {

        return $this->render('AppBundle:Default:reservation.html.twig');
    }

    /**
     * @Route("/{ruta}", name="seccion_view")
     */
    public function viewAction($ruta) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaAdministracionBundle:Seccion')->findOneByRuta($ruta);

        return $this->render('AppBundle:Default:seccion.html.twig', array(
                    'entity' => $entity,
                        )
        );
    }

    /**
     * @Route("/change-language/{lang}", name="change_language")
     */
    public function changeLanguajeAction($lang, \Symfony\Component\HttpFoundation\Request $request) {

        $request->getSession()->set('_locale', $lang);
        return $this->redirect($this->generateUrl('homepage'));
    }

}
